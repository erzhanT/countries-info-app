import React from 'react';
import './CountriesData.css';

const CountriesData = props => {
    console.log(props.countryInfo);
    console.log(props.borders);
    return (
        <div className="countries-data">
            <div>
                <p>Country: <strong>{props.countryInfo.name}</strong></p>
               <p>Flag: <img className="flag" src={props.countryInfo['flag']} alt={props.countryInfo['name']}/></p>
                <p>Capital: <strong>{props.countryInfo['capital']}</strong></p>
                <p>Population: {props.countryInfo['population']} people</p>
                <p>Borders with: </p>
                <ul>
                    {props.borders.map((item , index) => {
                        return (
                            <li
                            key={index}
                            >{item.name}</li>
                        )
                    })}
                </ul>
            </div>
        </div>
    );
};

export default CountriesData;