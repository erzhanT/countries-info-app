import React from 'react';
import './CountriesList.css';
const CountriesList = props => {

    return (
        <div className="countries-list">
            <li><button
                className="country"
                onClick={props.onClickHandler}
            >
                {props.name}
            </button></li>
        </div>
    );
};

export default CountriesList;