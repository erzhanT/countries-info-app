import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './containers/App/App';

// axios.defaults.baseURL = 'https://restcountries.eu/rest/v2/';

ReactDOM.render(<App />, document.getElementById('root'));

